import { Component, OnInit } from "@angular/core";
import { NgForm, FormBuilder, Validators, FormGroup } from "@angular/forms";
import { AuthenticationService } from "./../../service/authentication.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-admin-signin",
  templateUrl: "./admin-signin.component.html",
  styleUrls: ["./admin-signin.component.scss"]
})
export class AdminSigninComponent implements OnInit {
  adminSignInForm: FormGroup;
  errorMsg: string;
  constructor(
    private formbuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.adminSignInForm = this.formbuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.required, Validators.pattern(/[0-9a-zA-Z{6,}]/)]
      ]
    });
  }
  onAuth() {
    const email = this.adminSignInForm.get("email").value;
    const password = this.adminSignInForm.get("password").value;
    console.log(email + " " + password);
    this.authenticationService.signInUser(email, password).then(
      data => {
        console.log(data);
        this.router.navigate(["/admin", "dashboard"]);
      },
      error => {
        this.errorMsg = "erreur de navigation";
        alert(this.errorMsg);
      }
    );
  }
  // onSubmitAdminSigninForm() {
  //   const email = this.adminSignInForm.get("email").value;
  //   const password = this.adminSignInForm.get("password").value;
  //   console.log();
  //   this.authenticationService
  //     .signUpUser(email, password)
  //     .then(() => {
  //       console.log("okey");
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }
}
