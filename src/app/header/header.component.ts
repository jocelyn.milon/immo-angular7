import { Component, OnInit } from "@angular/core";
import * as firebase from "firebase";
import { AuthenticationService } from "../service/authentication.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  loggedIn: boolean = false;

  constructor(private authentificationService: AuthenticationService) {}

  ngOnInit() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
  }
  onSignOut() {
    this.authentificationService.signOutUser();
  }
}
