import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminSigninComponent } from "./admin/admin-signin/admin-signin.component";
import { AdminDashboardComponent } from "./admin/admin-dashboard/admin-dashboard.component";
import { HomeComponent } from "./home/home.component";
import { SigninComponent } from "./authentification/signin/signin.component";
import { SinglePropertyComponent } from "./single-property/single-property.component";
import { AuthGuardService } from "./service/auth-guard.service";

const routes: Routes = [
  { path: "home", component: HomeComponent },
  {
    path: "admin/login",
    canActivate: [AuthGuardService],
    component: AdminSigninComponent
  },
  { path: "admin/dashboard", component: AdminDashboardComponent },
  { path: "login", component: SigninComponent },
  { path: "property/:id", component: SinglePropertyComponent },
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "**", redirectTo: "home" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule {}
