import { Component } from "@angular/core";
import * as firebase from "firebase";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "immo";

  constructor() {
    // Your web app's Firebase configuration
    const firebaseConfig = {
      apiKey: "AIzaSyBY6sSXE7kuIwGy5TBpK79v6N4mVDtXDls",
      authDomain: "immo-4be31.firebaseapp.com",
      databaseURL: "https://immo-4be31.firebaseio.com",
      projectId: "immo-4be31",
      storageBucket: "immo-4be31.appspot.com",
      messagingSenderId: "140539037728",
      appId: "1:140539037728:web:94cd2f3966c527d8"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
