import { Component, OnInit, OnDestroy } from "@angular/core";
import { PropertiesService } from "./../service/properties.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, OnDestroy {
  properties = [];
  propertiesSubscription: Subscription;

  constructor(private PropertiesService: PropertiesService) {}

  ngOnInit() {
    this.propertiesSubscription = this.PropertiesService.propertiesSubject.subscribe(
      (data: any) => {
        this.properties = data;
      }
    );
    this.PropertiesService.getProperties();
    this.PropertiesService.emitProperties();
  }

  ngOnDestroy() {
    this.propertiesSubscription.unsubscribe();
  }

  getSoldValue(index) {
    if (this.properties[index].sold) {
      return "red";
    } else {
      return "green";
    }
  }
}
