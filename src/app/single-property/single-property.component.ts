import { Component, OnInit } from "@angular/core";
import { PropertiesService } from "./../service/properties.service";
import { ActivatedRoute } from "@angular/router";
import { Property } from "./../interfaces/property";

@Component({
  selector: "app-single-property",
  templateUrl: "./single-property.component.html",
  styleUrls: ["./single-property.component.scss"]
})
export class SinglePropertyComponent implements OnInit {
  property: Property;

  constructor(
    private route: ActivatedRoute,
    private propertiesService: PropertiesService
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id");
    this.propertiesService
      .getSingleProperties(id)
      .then((property: Property) => {
        this.property = property;
      })
      .catch(error => {
        console.error(error);
      });
  }
}
