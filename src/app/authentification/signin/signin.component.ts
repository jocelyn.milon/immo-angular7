import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { Validators } from "@angular/forms";
import { AuthenticationService } from "./../../service/authentication.service";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"]
})
export class SigninComponent implements OnInit {
  signinForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() {
    this.initSigninForm();
  }
  initSigninForm() {
    this.signinForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }
  onSubmitSigninForm() {
    const email = this.signinForm.get("email").value;
    const password = this.signinForm.get("password").value;
    console.log();
    this.authenticationService
      .signUpUser(email, password)
      .then(() => {
        console.log("okey");
      })
      .catch(error => {
        console.log(error);
      });
  }
}
